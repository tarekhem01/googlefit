import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, PermissionsAndroid } from 'react-native';
import GoogleFit, { Scopes } from 'react-native-google-fit'
import { withNavigationFocus } from "react-navigation";

export default class Fit extends Component {
    constructor(props) {
        super(props);
        this.state = {
            show: false,
            show1: false,
            steps: 0,
            date: '2020-01-15',
            weight: 0
        };

    }


    async componentDidMount() {
        const options = {
            scopes: [
                Scopes.FITNESS_ACTIVITY_READ,
                Scopes.FITNESS_ACTIVITY_READ_WRITE,
                Scopes.FITNESS_BODY_READ,
                Scopes.FITNESS_BODY_READ_WRITE,
            ],
        }

        try {
            const granted = await PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION
            )
            if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                GoogleFit.authorize(options)
                    .then(authResult => {
                        console.log('auth successful')
                    })
                    .catch(() => {
                        console.log('auth error')
                    })
            }
        } catch (err) {
            Reactotron.log("problemi")
        }


    }
    getCount = async () => {
        let { show, steps } = this.state
        show = true
        const range = {
            startDate: "2020-01-15T00:00:17.971Z", // required ISO8601Timestamp
            endDate: new Date().toISOString(), // required ISO8601Timestamp
            bucketUnit: "DAY", // optional - default "DAY". Valid values: "NANOSECOND" | "MICROSECOND" | "MILLISECOND" | "SECOND" | "MINUTE" | "HOUR" | "DAY"
            bucketInterval: 1, // optional - default 1. 

        };

        await GoogleFit.getDailyStepCountSamples(range)
            .then((res) => {
                if (res.length > 0) {
                    const countData = res.find((data) => data.source === 'com.google.android.gms:estimated_steps')
                    console.log('count Data', countData)
                    steps = countData[0].value
                }
            })
            .catch((err) => { console.warn(err) })
        this.setState({ show ,steps})

    }
    getWeight = async () => {
        let { show1, weight } = this.state
        show1 = true
        const opt = {
            unit: "pound", // required; default 'kg'
            startDate: "2020-01-15T00:00:17.971Z", // required
            endDate: new Date().toISOString(), // required
            ascending: false // optional; default false
        };

        GoogleFit.getWeightSamples(opt, (err, res) => {
            if (res.length > 0) {
                weight = res[0].value
            }
        });
        this.setState({ show1, weight })
    }
    render() {
        let { show, show1, steps, date, weight } = this.state
        return (
            <View style={styles.container}>
                <View style={{ flexDirection: "row" }}>
                    <TouchableOpacity style={styles.getcount} onPress={() => this.getCount()}>
                        <Text style={{ color: "#fff" }}>Get Daily Steps</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.getcount} onPress={() => this.getWeight()}>
                        <Text style={{ color: "#fff" }}>Get Daily Weight</Text>
                    </TouchableOpacity>
                </View>

                {
                    show == true ?
                        <Text>You walk {steps} steps on {date} </Text>
                        :
                        null
                }
                {
                    show1 == true ?
                        <Text>Your weight is {weight} kg on  {date} </Text>
                        :
                        null
                }
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
    getcount: {
        height: "4%",
        backgroundColor: "#ff6f61",
        padding: "5%",
        justifyContent: "center",
        alignItems: "center",
        borderRadius: 20,
        marginRight: "3%"
    }
});

